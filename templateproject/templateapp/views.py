from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
# Create your views here.
class use_template(TemplateView):
    template_name="templateapp/home.html"
class about_template(TemplateView):
    template_name="templateapp/about.html"
class contact_template(TemplateView):
    template_name="templateapp/contact.html"
